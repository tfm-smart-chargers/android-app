package com.upc.tfm.smartchargers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by @crserran (Gitlab / Github) on 12/10/2019.
 */

public class DataActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);

        String textDate = "";
        Double charge = 0.0;

        // Get the transferred data from source activity.
        Intent intent = getIntent();
        String registers = intent.getStringExtra("registers");

        Boolean charging = false;
        JSONArray registersArray = null;
        String iDate = "", iTime = "";
        String cDate = "", cTime = "";
        try {
            registersArray = new JSONArray(registers);
            for (int i = 0; i < registersArray.length(); i++) {
                JSONObject register = registersArray.getJSONObject(i);
                Integer flag = register.getInt("flag");
                String date = register.getString("date");
                String time = register.getString("time");
                Double c = register.getDouble("charge");

                if (flag == 0 || flag == 1) {
                    textDate = getTextFlag(flag, date, time);
                    Log.i("-", "Text date: " + textDate);
                    if (flag == 0) {
                        iDate = date;
                        iTime = time;
                    }
                } else if (flag == 2) {
                    Log.i("-", "Charge: " + c);
                    charge = c;
                    cDate = date;
                    cTime = time;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TextView dateTextView = findViewById(R.id.date);
        dateTextView.setText(textDate);

        TextView chargeTextView = findViewById(R.id.charge);
        chargeTextView.setText(charge + " %");

        ImageView battery = findViewById(R.id.battery);
        if (charge < 20) {
            battery.setImageResource(R.drawable.ic_battery_empty);
        } else if (charge >= 20 && charge < 40) {
            battery.setImageResource(R.drawable.ic_battery_low);
        } else if (charge >= 40 && charge < 60) {
            battery.setImageResource(R.drawable.ic_battery_medium);
        } else if (charge >= 60 && charge < 80) {
            battery.setImageResource(R.drawable.ic_battery_high);
        } else if (charge >= 80 && charge < 100) {
            battery.setImageResource(R.drawable.ic_battery_full);
        }

        if (cDate != "") {
            charging = isCharging(iDate, iTime, cDate, cTime);
        }
        if(charging) {
            ImageView chargingImage = findViewById(R.id.charging);
            chargingImage.setImageResource(R.mipmap.ic_charge);
        }
    }

    private String getTextFlag(Integer flag, String date, String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Date parsed = null;
        try {
            parsed = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(parsed);

        StringBuilder sb = new StringBuilder();
        sb.append("Fecha de ");
        switch(flag) {
            case 0:
                sb.append("entrada: ");
                break;
            case 1:
                sb.append("salida: ");
                break;
            case 2:
                sb.append("carga: ");
                break;
            case -1:
                Log.e("-", "Error obtaining flag");
                break;
        }

        final String dateTimeFormatString = "EEEE, d 'de' MMMM ";
        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DATE) == cal.get(Calendar.DATE) ) {
            sb.append("Hoy ");
        } else if (now.get(Calendar.DATE) - cal.get(Calendar.DATE) == 1  ){
            sb.append("Ayer ");
        } else if (now.get(Calendar.YEAR) == cal.get(Calendar.YEAR)) {
            sb.append(DateFormat.format(dateTimeFormatString, cal).toString());
        } else {
            sb.append(DateFormat.format("MMMM dd yyyy ", cal).toString());
        }
        sb.append("a las " + time);
        return sb.toString();
    }

    private Boolean isCharging(String iDate, String iTime, String cDate, String cTime) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        Date inputDate = null;
        Date chargeDate = null;
        try {
            inputDate = format.parse(iDate);
            chargeDate = format.parse(cDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar inputCal = Calendar.getInstance();
        inputCal.setTime(inputDate);
        Calendar chargeCal = Calendar.getInstance();
        chargeCal.setTime(chargeDate);

        if(inputCal.get(Calendar.DATE) < chargeCal.get(Calendar.DATE)) {
            return true;
        } else if (inputCal.get(Calendar.DATE) == chargeCal.get(Calendar.DATE)) {
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            Date inputTime = null;
            Date chargeTime = null;
            try {
                inputTime = timeFormat.parse(iTime);
                chargeTime = timeFormat.parse(cTime);

                if (inputTime.before(chargeTime)) {
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        return false;
    }
}
