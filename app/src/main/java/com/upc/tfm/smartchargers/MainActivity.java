package com.upc.tfm.smartchargers;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by @crserran (Gitlab / Github) on 12/10/2019.
 */

public class MainActivity extends AppCompatActivity {
    private final String HOST = "http://34.76.91.105:8080/api";

    private EditText editText;
    private TextView errorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        errorTextView = findViewById(R.id.error);
        errorTextView.setVisibility(View.INVISIBLE);
        editText = findViewById(R.id.campo_edit_text);

        final Button sendButton = findViewById(R.id.send_btn);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String carPlate = editText.getText().toString();

                sendCarPlate(carPlate);
            }
        });
    }

    private void sendCarPlate(String carPlate) {
        Log.i(".","Requesting: " + carPlate);

        String URL = HOST + "/registers/" + carPlate;

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET,
                URL,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("OK", response.toString());

                if(response.length() > 0) {
                    Intent intent = new Intent(MainActivity.this, DataActivity.class);
                    intent.putExtra("registers", response.toString());
                    startActivity(intent);
                } else {
                    errorTextView.setVisibility(View.VISIBLE);
                    Timer t = new Timer(false);
                    t.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                public void run() {
                                    errorTextView.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    }, 3000);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorTextView.setVisibility(View.VISIBLE);
                Timer t = new Timer(false);
                t.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                errorTextView.setVisibility(View.INVISIBLE);
                            }
                        });
                    }
                }, 3000);
                Log.e("FAIL", error.toString());
            }
        }
        );

        VolleyController.getInstance(this).addToQueue(jsonObjectRequest);
    }
}
